#!/bin/bash

# Installation script for the XS-treatment SAXS data treatment suite.
# by Dennys Reis <dreis@if.usp.br> and
# Arnaldo G. Oliveira-Filho <agolivei@.if.usp.br>.
# 2022-09-14. Licensed under GPL.

# Introduction message, about the suite.
intro_message()
{
cat <<EOF
 XS-treatment suite, for SAXS / WAXS data treatment , developed by 
 Dennys Reis <dreis@if.usp.br> and Arnaldo G. Oliveira-Filho <agolivei@.if.usp.br>
 at Complex Fluids Group - IF - USP.

 This software is under GPL. 

 The current version of the suite (${XSver}, on ${DATE}) was tested under ${PYver}.
EOF
}

# How to use the install script.
help_message()
{
    cat <<EOF

Usage:
 ./install.sh [-u]

 -h : show this message and exit
 -u : uninstall current version
EOF
}

# Explains what will be done along installation process.
procedure_message()
{
cat <<EOF

 This installation script firstly tests whether the necessary ${PYver} modules
 can be correctly imported. If successful, it tries to install the treatment
 programs in ${stddir} (or other place of your preference) and create links for
 the executables under ${stddir}.
EOF
}

# Check whether libraries are correctly imported.
check_libraries()
{
    # List of failed imports.
    faillist=.fail_list.dat
    rm -f ${faillist}

    libs=$(egrep -h '^import|^from.*import' *.py | \
	   egrep -v XS | \
	   sed -e 's/\ as.*//' | \
	   sort -u | \
	   gawk '{ print $2 }' )

    # Check Python 3 and suite demanded libraries installation.
    pt3=$(which python3)
    if [ -x "${pt3}" ] ; then

        # Found Python 3.
        echo -e "\n * Python 3 found as ${pt3}.\n\n Library checking: \n"
        
        # Run Python 3 module checker.
        ${pt3} ./XS_A_checklib.py ${faillist} ${libs}
    else
        echo -e "\n ERROR: No Python 3 executable found."
        echo " Please, check your installation and retry."
        exit 1
    fi

    # 
    nlibs=$(wc -w ${faillist} | gawk '{print $1}')
    if [ $nlibs -ne 0 ] ; then
	echo -e "\n ** WARNING: The following libraries could not be correctly imported:"
	cat ${faillist}
	echo -ne "\n Please, install the Python 3 packages correspondent" 
	echo " to these libraries and rerun this script."
	exit 1
    fi
}

# Installation directory.
get_install_dir()
{
    echo -n " Type another directory or [Enter] to proceed: "
    #
    while
	read newdir
	[ -z $newdir ] || stddir=$newdir
	[ ! -d $stddir ] 
    do
	echo " ERROR: directory ${outdir} does not exist. Please, try again."
    done
    echo
}


# Check whether installation directory exists and create it.
create_installation_dir()
{
    # Check whether directory exists.
    if [ -d ${instdir} ] ; then
	echo -e "\n WARNING: Directory ${instdir} already exists."
	echo -n " Should I continue and overwrite its content? [y/N]"
	read cont
	[ "$cont" = "y" ] || exit 1
    else
	errmsg="\n I cannot create ${instdir}. May be you should run this script with sudo."
	# Try to create directory.
	mkdir ${instdir}
	if [ $? -ne 0 ] ; then
	    echo -e ${errmsg}
	    exit 1
	fi
    fi
}

# Update usage message in files.
update_info_message()
{
 msg=$(cat <<EOF 
 ${XSver} (${DATE}), ${Pv} \n \
\n This script is part of the XS-treatment suite for SAXS WAXS data treatment \
\n created by Arnaldo Oliveira \<agolivei@if.usp.br\> and Dennys Reis \<dreis@if.usp.br\>.
EOF
)
    # Update usage message in each file.
    for F in ${PYfiles} ; do
        sed -i "s/__version__\ *=.*/__version__   = \'${XSver}\'/" ${F}
        sed -i "s/__date__\ *=.*/__date__   = \'${DATE}\'/" ${F}
        sed -i "s/__status__\ *=.*/__status__   = \'${Status}\'/" ${F}
    done
}

# If installation directory is not standard, correct it in files.
subst_libs_in_files()
{
    cd ${instdir}
    withlib=$(grep ${instdir} * | cut -d: -f1 | sort -u)
    for F in withlib ; do
	sed -i "s,${olddir},${instdir}," ${F}
    done
}

# Uninstall procedure.
uninstall_all()
{
    intro_message
    echo -en "\n This procedure will uninstall the current version of"
    echo -en " the XS-treatment suite.\n Do you really want to proceed? [y/N] "
    read p
    [ "$p" = "y" ] || exit 0

    # Search for executables.
    tgtfiles=$(find ${bindir} -name XS\*py)
    lastfile=$(find ${bindir} -name XS\*py | tail -n 1)
    if [ "${tgtfiles}" = "" ] ; then
        echo -e "\n WARNING: No executable files found. Aborting."
        exit 1
    fi
    
    # Search for installation directory.
    thelink=$(readlink -qn -e ${lastfile} 2> /dev/null)
    instdir=$(dirname ${thelink} 2> /dev/null)
    if [ "${instdir}" = "" ] ; then
        echo -e "\n WARNING: No installation directory found. Aborting."
        exit 1
    fi
    
    instparent=$(dirname ${instdir})
    
    cat <<EOF

 I found the current version of the suite is installed 
 in directory ${instdir}.

 *** Type [Enter] if this is the version you want to exclude OR
 type the target directory:
EOF

    # Ask and search target directory.
    while
	read targetdir
	[ -z $targetdir ] || instdir=$targetdir
	[ ! -d $instdir ]
    do
	echo -e "\n ERROR: directory ${instdir} does not exist. Please, try again."
    done

    # Check whether files exist in installation directory.
    echo
    for tf in ${tgtfiles} ; do
        tfile=$(basename ${tf})
        echo -n " Searching for ${tfile} in ${instdir} ..."
        rf=$(find ${instdir} -maxdepth 1 -name ${tfile} -exec echo 1 \;)
        if [ "$rf" != "1" ] ; then
	    echo -e "\n Sorry, I could not find file $tfile."
	    echo -e " Please, find the correct installation directory and rerun this script."
	    exit 1
        fi
        echo " done."
    done
    
    # Delete links to executables.
    echo -en "\n * Removing the links to executables: "
    for F in ${PYfiles} ${OTfiles} ; do
	rm -f ${bindir}/${F}
    done
    # Delete installation directory and link to it.
    echo -en " done.\n\n * Removing the installation directory: "
    rm -rf ${instdir}
    echo -en " done.\n\n * Removing the link to the installation directory: "
    rm -f ${instparent}/${outdir}
    echo -en " done.\n"
    
    echo -e "\n Uninstall procedure completed."
}


#
# Main.  

#
# Header info.
__version__='2.4.1'
__author__='Dennys Reis & Arnaldo G. Oliveira-Filho'
__credits__='Dennys Reis & Arnaldo G. Oliveira-Filho'
__email__='dreis@if.usp.br ,agolivei@if.usp.br'
__license__='GPL'
__date__='2022-09-14'
__status__='Development'
__copyright__='Copyright 2022 by GFCx-IF-USP'
__local__='GFCx-IF-USP'

# Standard installation directory.
bindir="/usr/local/bin"
stddir="/usr/local/lib"
outdir="XS-treatment"

# Files to be installed.
PYfiles="XS_A_checklib.py XS_azimuthalaverage.py XS_curveadd.py XS_curveplot.py XS_extractlog.py XS_getbeamcenter.py XS_help.py XS_imageadd.py XS_imagemerge.py XS_imageplot.py XS_libazimuthalaverage.py XS_libfileutils.py XS_libgetbeamcenter.py XS_libimageplot.py XS_libnormalizations.py XS_libreadparameters.py XS_maskconv.py XS_menu.py XS_ponifile.py XS_radialaverage.py clponize.py"

# Current version.
DATE="2022-09-14"
PYver="Python 3.10.7"
XSver="2.4.1"
Status=${__status__}

# Check command line option. If -u, uninstall.
if [ -n "$1" ] ; then
    case $1 in
	-h)
	    intro_message ${XSver} ${DATE} ${PYver}
	    help_message
	    ;;
	
	-u)
	    uninstall_all ${PYfiles} ${OTfiles} ${outdir} ${bindir}
	    ;;
	
	*)
	    echo "Option not recognized."
	    help_message
	    exit 1
    esac
    exit 0
fi


### Intro message.
intro_message ${DATE} ${PYver} ${XSver}
procedure_message ${stddir}

# Confirm installation.
echo -n " Proceed? [Y/n] "
read p
[ "$p" = "n" ] && exit 1

### Check libraries.
echo -e "\n1. Checking necessary libraries for installation."
check_libraries
echo -e "\n All necessary libraries were correctly imported. "
rm -f ${faillist}

# Check todos (to MS-DOS) converter.
td=$(which todos)
if [ "$td" = '' ] ; then
    echo -e "\n ** WARNING: todos (MS-DOS converter) not found. "
    echo -e "  File lines may lack CRLF terminators in MS environments.\n"
fi

### Copy files to standard directory.
echo -e "\n2. Standard installation will copy files to ${outdir}."
# Define standard installation directory.
olddir=${stddir}/${outdir}
get_install_dir ${stddir}
instdir=${stddir}/${outdir}-2.4.1
dirlink=${stddir}/${outdir}

# Create installation directory and copy files to it.
echo -en "\n3. Creating destination directory..."
create_installation_dir ${instdir}
echo -e " done.\n"

# Update version information.
Pv=$(python3 -V)  # Python version
echo -e "\n4. Updating version information in files..."
update_info_message ${PYfiles} ${OTfiles} ${XSver} ${DATE} ${Pv} ${Status}
echo -e " done.\n"

# Copy files to installation directory.
echo -e "\n5. Copying files to destination directory..."
\cp -av ${PYfiles} ${OTfiles} ${instdir}/
echo -e " done.\n"

# Link to current version directory.
echo -ne "\n6. Link to directory..."
rm -rf ${dirlink}  # Remove old link
ln -sf ${instdir} ${dirlink}
echo " done."

# Correct library calls if necessary.
if [ "${dirlink}" != "${olddir}" ] ; then
    echo -e "\n(*) Adjusting library paths..."
    subst_libs_in_files ${instdir}
    echo -e " done.\n"
fi

# Links to all executables.
echo -en "\n7. Creating links to executables... "
for F in $PYfiles ; do
    ln -sf ${dirlink}/$F ${bindir}/$F
done
echo " done."

echo -e "\n Installation ended successfully."
